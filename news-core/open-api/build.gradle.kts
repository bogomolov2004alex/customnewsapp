plugins {
  alias(libs.plugins.jetbrains.kotlin.jvm)
  alias(libs.plugins.kotlin.serialization)
  alias(libs.plugins.kapt)
}

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

dependencies {
  
  implementation(libs.androidx.annotation)
  
  implementation(libs.retrofit)
  implementation(libs.retrofit.converter.kotlinx.serialization)
  implementation(libs.retrofit.adapters.result)
  
  implementation(libs.kotlinx.coroutines.core)
  
  api(libs.okhttp)
  
  api(libs.kotlinx.serialization.json)
  
  kapt(libs.retrofit.response.type.keeper)
}
