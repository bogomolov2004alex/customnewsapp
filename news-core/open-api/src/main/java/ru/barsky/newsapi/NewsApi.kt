@file:Suppress("unused")

package ru.barsky.newsapi

import androidx.annotation.IntRange
import com.skydoves.retrofit.adapters.result.ResultCallAdapterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.kotlinx.serialization.asConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query
import ru.barsky.newsapi.models.ArticleDTO
import ru.barsky.newsapi.models.Language
import ru.barsky.newsapi.models.ResponseDTO
import ru.barsky.newsapi.models.SortBy
import ru.barsky.newsapi.utils.NewsApiKeyInterceptor
import java.util.Date

/**
 * [API documentation](https://newsapi.org/docs/get-started)
 */
interface NewsApi {
  
  /**
   * [API details](https://newsapi.org/docs/endpoints/everything)
   */
  @Suppress("LongParameterList")
  @GET("everything")
  suspend fun everything(
    @Query("q") query: String? = null,
    @Query("from") from: Date? = null,
    @Query("to") to: Date? = null,
    @Query("language") languages: List<@JvmSuppressWildcards Language>? = null,
    @Query("sortBy") sortBy: SortBy? = null,
    @Query("pageSize") @IntRange(from = 0, to = 100) pageSize: Int = 100,
    @Query("page") @IntRange(from = 1) page: Int = 1,
  ): Result<ResponseDTO<ArticleDTO>>
}

/**
 * TODO: Create custom result adapter factory for retrofit
 */
@Suppress("ForbiddenComment")
fun NewsApi(
  baseUrl: String,
  apiKey: String,
  okHttpClient: OkHttpClient? = null,
  json: Json = Json,
): NewsApi {
  val modifiedOkHttpClient =
    (okHttpClient?.newBuilder() ?: OkHttpClient.Builder())
      .addInterceptor(NewsApiKeyInterceptor(apiKey))
      .build()
  
  val retrofit = Retrofit.Builder()
    .baseUrl(baseUrl)
    .client(modifiedOkHttpClient)
    .addConverterFactory(
      json.asConverterFactory("application/json".toMediaType()),
    )
    .addCallAdapterFactory(ResultCallAdapterFactory.create())
    .build()
  
  return retrofit.create()
}
