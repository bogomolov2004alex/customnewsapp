package ru.barsky.newsapi.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
enum class Language {
  @SerialName("en")
  EN,
  
  @SerialName("ru")
  RU,
  
  @SerialName("de")
  DE,
  
  @SerialName("fr")
  FR,
  
  @SerialName("es")
  ES,
  
  @SerialName("ar")
  AR,
  
  @SerialName("it")
  IT,
  
  @SerialName("nl")
  NL,
  
  @SerialName("he")
  HE,
  
  @SerialName("no")
  NO,
  
  @SerialName("pt")
  PT,
  
  @SerialName("sv")
  SV,
  
  @SerialName("ud")
  UD,
  
  @SerialName("zh")
  ZH,
}
