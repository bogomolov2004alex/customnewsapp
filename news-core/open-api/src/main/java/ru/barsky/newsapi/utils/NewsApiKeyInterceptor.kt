package ru.barsky.newsapi.utils

import okhttp3.Interceptor
import okhttp3.Response

internal class NewsApiKeyInterceptor(
  private val apiKey: String,
) : Interceptor {
  override fun intercept(chain: Interceptor.Chain): Response =
    chain.run {
      val request = request().newBuilder().url(
        request().url.newBuilder()
          .addQueryParameter("apiKey", apiKey)
          .build(),
      ).build()
      
      proceed(request)
    }
}
