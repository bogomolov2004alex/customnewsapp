import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

plugins {
  alias(libs.plugins.android.library)
  alias(libs.plugins.jetbrains.kotlin.android)
}

kotlin {
  explicitApi = ExplicitApiMode.Strict
}

android {
  namespace = "ru.barsky.news.data"
  compileSdk = libs.versions.androidCompileSdk.get().toInt()
  
  defaultConfig {
    minSdk = libs.versions.androidMinSdk.get().toInt()
  }
  
  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = "1.8"
  }
}

dependencies {
  
  implementation(projects.newsCore.common)
  implementation(projects.newsCore.database)
  implementation(projects.newsCore.openApi)
  
  implementation(libs.androidx.core.ktx)
  
  implementation(libs.kotlinx.coroutines.android)
  
  implementation(libs.javax.inject)
}
