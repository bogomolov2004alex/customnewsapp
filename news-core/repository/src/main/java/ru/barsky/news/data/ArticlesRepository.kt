package ru.barsky.news.data

import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.onEach
import ru.barsky.news.common.Logger
import ru.barsky.news.data.models.Article
import ru.barsky.news.database.NewsDatabase
import ru.barsky.news.database.models.ArticleDBO
import ru.barsky.newsapi.NewsApi
import ru.barsky.newsapi.models.ArticleDTO
import ru.barsky.newsapi.models.ResponseDTO
import javax.inject.Inject

public class ArticlesRepository @Inject constructor(
  private val database: NewsDatabase,
  private val api: NewsApi,
  private val logger: Logger,
) {
  
  /**
   * Получение актуальных новостей с отслеживанием состояния запроса ("В процессе", "Успшено", "Ошибка")
   */
  @OptIn(ExperimentalCoroutinesApi::class)
  public fun getAll(
    query: String,
    mergeStrategy: MergeStrategy<RequestResult<List<Article>>> = DefaultMergeStrategy()
  ): Flow<RequestResult<List<Article>>> {
    val cachedArticles: Flow<RequestResult<List<Article>>> = getAllFromDatabase()
    val remoteArticles: Flow<RequestResult<List<Article>>> = getAllFromServer(query)
    
    return cachedArticles.combine(remoteArticles, mergeStrategy::merge)
      .flatMapLatest { result ->
        if (result is RequestResult.Success) {
          database.articlesDao.observeAll()
            .map { articleDbo -> articleDbo.map { it.toArticle() } }
            .map { RequestResult.Success(it) }
        } else {
          flowOf(result)
        }
      }
  }
  
  private fun getAllFromServer(query: String): Flow<RequestResult<List<Article>>> {
    val apiRequest = flow { emit(api.everything(query = query)) }
      .onEach { result ->
        if (result.isSuccess) {
          saveArticlesToCache(result.getOrThrow().articles)
        }
      }
      .onEach { result ->
        if (result.isFailure) {
          logger.e(
            LOG_TAG,
            "Error fetching articles from server.\nCause: ${result.exceptionOrNull()}",
          )
        }
      }
      .map { it.toRequestResult() }
    
    val start = flowOf<RequestResult<ResponseDTO<ArticleDTO>>>(RequestResult.InProgress())
    
    return merge(apiRequest, start)
      .map { result ->
        result.map { response ->
          response.articles.map { it.toArticle() }
        }
      }
  }
  
  private suspend fun saveArticlesToCache(data: List<ArticleDTO>) =
    data.map { articleDto -> articleDto.toArticleDbo() }
      .let { articles -> database.articlesDao.insert(articles) }
  
  private fun getAllFromDatabase(): Flow<RequestResult<List<Article>>> {
    val dbRequest = database.articlesDao::getAll.asFlow()
      .map<List<ArticleDBO>, RequestResult<List<ArticleDBO>>> { RequestResult.Success(it) }
      .catch {
        logger.e(LOG_TAG, "Error getting articles from database\nCause: ${it.message}")
        emit(RequestResult.Error(error = it))
      }
    
    val start = flowOf<RequestResult<List<ArticleDBO>>>(RequestResult.InProgress())
    
    return merge(start, dbRequest)
      .map { result ->
        result.map { articlesDbo ->
          articlesDbo.map { it.toArticle() }
        }
      }
  }
  
  private companion object {
    const val LOG_TAG = "ArticlesRepository"
  }
}
