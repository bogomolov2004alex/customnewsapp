package ru.barsky.news.data

import ru.barsky.news.data.models.Article
import ru.barsky.news.data.models.Source
import ru.barsky.news.database.models.ArticleDBO
import ru.barsky.newsapi.models.ArticleDTO
import ru.barsky.newsapi.models.SourceDTO
import ru.barsky.news.database.models.Source as SourceDBO

internal fun ArticleDBO.toArticle(): Article =
  Article(
    cacheId = id,
    source = source.toSource(),
    author = author,
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage,
    publishedAt = publishedAt,
    content = content,
  )

internal fun ArticleDTO.toArticle(): Article =
  Article(
    source = source.toSource(),
    author = author,
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage,
    publishedAt = publishedAt,
    content = content,
  )

internal fun ArticleDTO.toArticleDbo(): ArticleDBO =
  ArticleDBO(
    source = source.toSourceDbo(),
    author = author,
    title = title,
    description = description,
    url = url,
    urlToImage = urlToImage,
    publishedAt = publishedAt,
    content = content,
  )

internal fun SourceDBO.toSource(): Source =
  Source(
    id = id,
    name = name,
  )

internal fun SourceDTO.toSource(): Source =
  Source(
    id = id ?: name,
    name = name,
  )

internal fun SourceDTO.toSourceDbo(): SourceDBO =
  SourceDBO(
    id = id ?: name,
    name = name,
  )
