@file:Suppress("UNUSED_PARAMETER")

package ru.barsky.news.data

import ru.barsky.news.data.RequestResult.Error
import ru.barsky.news.data.RequestResult.InProgress
import ru.barsky.news.data.RequestResult.Success

public interface MergeStrategy<E> {
  
  public fun merge(left: E, right: E): E
}

internal class DefaultMergeStrategy<T : Any> : MergeStrategy<RequestResult<T>> {
  
  @Suppress("CyclomaticComplexMethod")
  override fun merge(left: RequestResult<T>, right: RequestResult<T>): RequestResult<T> =
    when {
      left is InProgress && right is InProgress -> merge(left, right)
      left is InProgress && right is Success -> merge(left, right)
      left is InProgress && right is Error -> merge(left, right)
      left is Success && right is InProgress -> merge(left, right)
      left is Success && right is Success -> merge(left, right)
      left is Success && right is Error -> merge(left, right)
      left is Error && right is InProgress -> merge(left, right)
      left is Error && right is Success -> merge(left, right)
      
      else -> error("Unimplemented branch left=$left & right=$right")
    }
  
  private fun merge(cache: InProgress<T>, server: InProgress<T>): RequestResult<T> =
    when {
      server.data != null -> server
      else -> cache
    }
  
  private fun merge(cache: InProgress<T>, server: Success<T>): RequestResult<T> =
    server
  
  private fun merge(cache: InProgress<T>, server: Error<T>): RequestResult<T> =
    Error(
      data = server.data ?: cache.data,
      error = server.error,
    )
  
  private fun merge(cache: Success<T>, server: InProgress<T>): RequestResult<T> =
    cache
  
  private fun merge(cache: Success<T>, server: Success<T>): RequestResult<T> =
    server
  
  private fun merge(cache: Success<T>, server: Error<T>): RequestResult<T> =
    Error(
      data = cache.data,
      error = server.error,
    )
  
  private fun merge(cache: Error<T>, server: InProgress<T>): RequestResult<T> =
    Error(
      data = server.data,
      error = cache.error,
    )
  
  private fun merge(cache: Error<T>, server: Success<T>): RequestResult<T> =
    server
}
