package ru.barsky.news.data

/**
 * RequestResult представляет собой запрос обновления данных,
 * который может происходить из нескольких источников
 */
public sealed class RequestResult<out E : Any>(public open val data: E? = null) {
  
  public class InProgress<E : Any>(data: E? = null) : RequestResult<E>(data)
  public class Success<E : Any>(override val data: E) : RequestResult<E>(data)
  public class Error<E : Any>(
    data: E? = null,
    public val error: Throwable? = null,
  ) : RequestResult<E>(data)
}

public fun <E : Any, T : Any> RequestResult<E>.map(mapper: (E) -> T): RequestResult<T> =
  when (this) {
    is RequestResult.InProgress -> RequestResult.InProgress(data?.let(mapper))
    is RequestResult.Success -> RequestResult.Success(mapper(data))
    is RequestResult.Error -> RequestResult.Error(data?.let(mapper))
  }

internal fun <T : Any> Result<T>.toRequestResult(): RequestResult<T> =
  when {
    isSuccess -> RequestResult.Success(getOrThrow())
    isFailure -> RequestResult.Error()
    else -> error("Impossible branch")
  }
