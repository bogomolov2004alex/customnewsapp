plugins {
  alias(libs.plugins.android.library)
  alias(libs.plugins.androidx.room)
  alias(libs.plugins.jetbrains.kotlin.android)
  alias(libs.plugins.ksp)
}

android {
  namespace = "ru.barsky.news.database"
  compileSdk = libs.versions.androidCompileSdk.get().toInt()
  
  defaultConfig {
    minSdk = libs.versions.androidMinSdk.get().toInt()
  }
  
  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = "1.8"
  }
}

room {
  schemaDirectory("${rootProject.projectDir}/schemas")
}

dependencies {
  
  implementation(libs.androidx.core.ktx)
  
  ksp(libs.androidx.room.compiler)
  implementation(libs.androidx.room.ktx)
}
