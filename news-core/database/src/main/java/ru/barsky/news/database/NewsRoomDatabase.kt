package ru.barsky.news.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.barsky.news.database.dao.ArticleDao
import ru.barsky.news.database.models.ArticleDBO
import ru.barsky.news.database.utils.Converters

class NewsDatabase internal constructor(
  private val database: NewsRoomDatabase
) {
  
  val articlesDao: ArticleDao
    get() = database.articlesDao()
}

@Database(entities = [ArticleDBO::class], version = 1)
@TypeConverters(Converters::class)
internal abstract class NewsRoomDatabase : RoomDatabase() {
  
  abstract fun articlesDao(): ArticleDao
}

fun NewsDatabase(context: Context): NewsDatabase =
  NewsDatabase(
    Room.databaseBuilder(
      checkNotNull(context.applicationContext),
      NewsRoomDatabase::class.java,
      "news",
    ).build(),
  )
