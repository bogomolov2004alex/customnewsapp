import org.jetbrains.kotlin.gradle.dsl.ExplicitApiMode

plugins {
  alias(libs.plugins.android.library)
  alias(libs.plugins.jetbrains.kotlin.android)
  alias(libs.plugins.dagger.hilt.android)
  alias(libs.plugins.kapt)
}

kotlin {
  explicitApi = ExplicitApiMode.Strict
}

android {
  namespace = "ru.barsky.news.main"
  compileSdk = libs.versions.androidCompileSdk.get().toInt()
  
  defaultConfig {
    minSdk = libs.versions.androidMinSdk.get().toInt()
  }
  
  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = "1.8"
  }
}

dependencies {
  
  api(projects.newsCore.repository)
  
  implementation(libs.kotlinx.coroutines.android)
  
  api(libs.kotlinx.immutable)
  
  implementation(libs.androidx.core.ktx)
  
  implementation(libs.androidx.lifecycle.runtime.ktx)
  implementation(libs.androidx.lifecycle.viewmodel.ktx)
  
  implementation(libs.dagger.hilt.android)
  kapt(libs.dagger.hilt.compiler)
}
