package ru.barsky.news.main

import kotlinx.collections.immutable.toImmutableList
import ru.barsky.news.data.RequestResult
import ru.barsky.news.data.models.Article

internal fun RequestResult<List<ArticleUI>>.toState(): State =
  when (this) {
    is RequestResult.InProgress -> State.Loading(data?.toImmutableList())
    is RequestResult.Success -> State.Success(data.toImmutableList())
    is RequestResult.Error -> State.Error(data?.toImmutableList())
  }

internal fun Article.toUiArticles(): ArticleUI =
  ArticleUI(
    id = cacheId,
    title = title,
    description = description,
    imageUrl = urlToImage,
    url = url,
  )
