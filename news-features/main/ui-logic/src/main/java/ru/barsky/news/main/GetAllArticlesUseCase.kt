package ru.barsky.news.main

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import ru.barsky.news.data.ArticlesRepository
import ru.barsky.news.data.RequestResult
import ru.barsky.news.data.map
import javax.inject.Inject

internal class GetAllArticlesUseCase @Inject constructor(
  private val repository: ArticlesRepository
) {
  
  operator fun invoke(query: String): Flow<RequestResult<List<ArticleUI>>> =
    repository.getAll(query)
      .map { requestResult ->
        requestResult.map { articles ->
          articles.map { it.toUiArticles() }
        }
      }
}
