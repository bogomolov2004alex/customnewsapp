package ru.barsky.news.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import ru.barsky.news.NewsTheme

@Composable
fun NewsMainScreen(modifier: Modifier = Modifier) {
  NewsMain(
    modifier = modifier,
    viewModel = viewModel(),
  )
}

@Composable
internal fun NewsMain(modifier: Modifier = Modifier, viewModel: NewsMainViewModel) {
  val state by viewModel.state.collectAsState()
  val currentState = state
  
  NewsMainContent(
    modifier = modifier,
    state = currentState,
  )
}

@Composable
private fun NewsMainContent(modifier: Modifier = Modifier, state: State) {
  Column(modifier) {
    when (state) {
      is State.None -> Unit
      is State.Loading -> ProgressIndicator(state)
      is State.Success -> ArticleList(state)
      is State.Error -> ErrorMessage(state)
    }
  }
}

@Composable
private fun ProgressIndicator(state: State.Loading) {
  Column {
    Box(
      modifier = Modifier
        .fillMaxWidth()
        .padding(8.dp),
      contentAlignment = Alignment.Center,
    ) {
      CircularProgressIndicator()
    }
    
    val articles = state.articles
    if (!articles.isNullOrEmpty()) {
      ArticleList(articles)
    }
  }
}

@Composable
private fun ErrorMessage(state: State.Error) {
  Column {
    Box(
      modifier = Modifier
        .fillMaxWidth()
        .background(NewsTheme.colorScheme.error)
        .padding(8.dp),
      contentAlignment = Alignment.Center,
    ) {
      Text(text = "Error during an update", color = NewsTheme.colorScheme.onError)
    }
  }
  
  val articles = state.articles
  if (!articles.isNullOrEmpty()) {
    ArticleList(articles)
  }
}
