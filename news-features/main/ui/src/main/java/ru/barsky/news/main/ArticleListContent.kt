package ru.barsky.news.main

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.key
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.compose.AsyncImagePainter
import ru.barsky.news.NewsTheme

@Composable
internal fun ArticleList(state: State.Success) {
  ArticleList(articles = state.articles)
}

@Preview
@Composable
internal fun ArticleList(
  @PreviewParameter(ArticlesPreviewProvider::class, limit = 1) articles: List<ArticleUI>
) {
  LazyColumn {
    items(articles) { article ->
      key(article.id) {
        Article(article)
      }
    }
  }
}

@Preview
@Composable
private fun Article(
  @PreviewParameter(ArticlePreviewProvider::class, limit = 1) article: ArticleUI
) {
  Row(Modifier.padding(bottom = 4.dp)) {
    article.imageUrl?.let { imageUrl ->
      var isImageVisible by remember { mutableStateOf(true) }
      
      if (!isImageVisible) return@let
      
      AsyncImage(
        model = imageUrl,
        onState = { state ->
          if (state is AsyncImagePainter.State.Error) {
            isImageVisible = false
          }
        },
        contentDescription = stringResource(R.string.content_desc_article_image),
        contentScale = ContentScale.Crop,
        modifier = Modifier.size(150.dp),
      )
    }
    Spacer(modifier = Modifier.size(4.dp))
    Column(modifier = Modifier.padding(8.dp)) {
      Text(
        text = article.title,
        style = NewsTheme.typography.headlineMedium,
        maxLines = 1,
      )
      Spacer(modifier = Modifier.size(4.dp))
      Text(
        text = article.description ?: "NO DESCRIPTION",
        style = NewsTheme.typography.bodyMedium,
        maxLines = 3,
      )
    }
  }
}

@Suppress("MagicNumber")
private class ArticlePreviewProvider : PreviewParameterProvider<ArticleUI> {
  
  override val values: Sequence<ArticleUI>
    get() = sequenceOf(
      ArticleUI(
        1,
        "Title 1",
        "Description 1",
        imageUrl = null,
        url = "",
      ),
      ArticleUI(
        2,
        "Title 2",
        "Description 2",
        imageUrl = null,
        url = "",
      ),
      ArticleUI(
        3,
        "Title 3",
        "Description 3",
        imageUrl = null,
        url = "",
      ),
    )
}

private class ArticlesPreviewProvider : PreviewParameterProvider<List<ArticleUI>> {
  
  private val articleProvider = ArticlePreviewProvider()
  
  override val values: Sequence<List<ArticleUI>>
    get() = sequenceOf(articleProvider.values.toList())
}
