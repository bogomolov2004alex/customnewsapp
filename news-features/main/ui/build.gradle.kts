plugins {
  alias(libs.plugins.android.library)
  alias(libs.plugins.jetbrains.kotlin.android)
  alias(libs.plugins.compose.compiler)
  alias(libs.plugins.dagger.hilt.android)
  alias(libs.plugins.kapt)
}

android {
  namespace = "ru.barsky.news.main"
  compileSdk = libs.versions.androidCompileSdk.get().toInt()
  
  defaultConfig {
    minSdk = libs.versions.androidMinSdk.get().toInt()
  }
  
  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = "1.8"
  }
  buildFeatures {
    compose = true
  }
  composeOptions {
    kotlinCompilerExtensionVersion = libs.versions.androidx.compose.kotlin.compiler.ext.get()
  }
}

dependencies {
  
  implementation(projects.newsCore.common)
  implementation(projects.newsCore.uikit)
  implementation(projects.newsFeatures.main.uiLogic)
  
  implementation(libs.androidx.core.ktx)
  
  implementation(libs.androidx.lifecycle.viewmodel.compose)
  
  implementation(libs.androidx.compose.runtime)
  
  implementation(platform(libs.androidx.compose.bom))
  
  implementation(libs.androidx.activity.compose)
  
  implementation(libs.coil.core)
  implementation(libs.coil.compose)
  
  implementation(libs.dagger.hilt.android)
  kapt(libs.dagger.hilt.compiler)
}
