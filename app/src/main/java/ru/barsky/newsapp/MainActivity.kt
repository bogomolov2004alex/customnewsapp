package ru.barsky.newsapp

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.ui.Modifier
import dagger.hilt.android.AndroidEntryPoint
import ru.barsky.news.NewsTheme
import ru.barsky.news.main.NewsMainScreen

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    enableEdgeToEdge()
    setContent {
      NewsTheme {
        Scaffold(modifier = Modifier.fillMaxSize()) { innerPadding ->
          NewsMainScreen(
            modifier = Modifier.padding(innerPadding),
          )
        }
      }
    }
  }
}
