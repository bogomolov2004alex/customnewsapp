package ru.barsky.newsapp

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import ru.barsky.news.common.AndroidLogcatLogger
import ru.barsky.news.common.AppDispatchers
import ru.barsky.news.common.Logger
import ru.barsky.news.database.NewsDatabase
import ru.barsky.newsapi.NewsApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
  
  @Provides
  @Singleton
  fun provideNewsApi(okHttpClient: OkHttpClient?): NewsApi =
    NewsApi(
      baseUrl = BuildConfig.NEWS_API_BASE_URL,
      apiKey = BuildConfig.NEWS_API_KEY,
      okHttpClient = okHttpClient,
    )
  
  @Provides
  @Singleton
  fun provideNewsDatabase(@ApplicationContext context: Context): NewsDatabase =
    NewsDatabase(context)
  
  @Provides
  @Singleton
  fun provideAppCoroutineDispatchers(): AppDispatchers = AppDispatchers()
  
  @Provides
  fun provideAndroidLogcatLogger(): Logger = AndroidLogcatLogger()
}
