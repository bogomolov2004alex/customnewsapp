package ru.barsky.newsapp

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class BuildTypeModule {
  
  @Provides
  @Singleton
  fun provideHttpClient(): OkHttpClient =
    HttpLoggingInterceptor().run {
      setLevel(HttpLoggingInterceptor.Level.BODY)
      
      OkHttpClient.Builder()
        .addInterceptor(this)
        .build()
    }
}