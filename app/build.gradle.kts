plugins {
  alias(libs.plugins.android.application)
  alias(libs.plugins.jetbrains.kotlin.android)
  alias(libs.plugins.compose.compiler)
  alias(libs.plugins.dagger.hilt.android)
  alias(libs.plugins.kapt)
  alias(libs.plugins.baselineprofile)
}

android {
  namespace = "ru.barsky.newsapp"
  compileSdk = libs.versions.androidCompileSdk.get().toInt()
  
  defaultConfig {
    applicationId = "ru.barsky.newsapp"
    minSdk = libs.versions.androidMinSdk.get().toInt()
    targetSdk = libs.versions.androidTargetSdk.get().toInt()
    versionCode = 1
    versionName = "1.0"
    
    testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    vectorDrawables {
      useSupportLibrary = true
    }
    
    buildConfigField("String", "NEWS_API_BASE_URL", "\"https://newsapi.org/v2/\"")
    buildConfigField("String", "NEWS_API_KEY", "\"e12a1a3dfab546789dbe71c4475b1674\"")
    
    resourceConfigurations += setOf("ru", "en")
    
    ndk {
      //noinspection ChromeOsAbiSupport
      abiFilters += setOf("armeabi-v7a", "arm64-v8a")
    }
  }
  
  signingConfigs {
    create("release") {
      storeFile = File(rootDir, "newsapp.keystore")
      storePassword = "example"
      keyAlias = "a.barsky"
      keyPassword = "example"
    }
  }
  
  buildTypes {
    release {
      signingConfig = signingConfigs["release"]
      isMinifyEnabled = false
      proguardFiles(
        getDefaultProguardFile("proguard-android-optimize.txt"),
        file("proguard/"),
      )
    }
  }
  compileOptions {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
  }
  kotlinOptions {
    jvmTarget = "1.8"
  }
  buildFeatures {
    buildConfig = true
    compose = true
  }
  composeOptions {
    kotlinCompilerExtensionVersion = libs.versions.androidx.compose.kotlin.compiler.ext.get()
  }
  packaging {
    resources {
      excludes += "/META-INF/{AL2.0,LGPL2.1}"
      excludes += "/META-INF/androidx.*.version"
      excludes += "/META-INF/com.google.*.version"
      excludes += "/META-INF/kotlinx_*.version"
      excludes += "/META-INF/com/android/build/gradle/app-metadata.properties"
      excludes += "/okhttp3/internal/publicsuffix/NOTICE"
      excludes += "/kotlin/**"
      excludes += "kotlin-tooling-metadata.json"
      excludes += "DebugProbesKt.bin"
    }
  }
}

dependencies {
  
  baselineProfile(projects.baselineprofile)
  
  implementation(projects.newsCore.common)
  implementation(projects.newsCore.database)
  implementation(projects.newsCore.openApi)
  implementation(projects.newsCore.repository)
  implementation(projects.newsCore.uikit)
  
  implementation(projects.newsFeatures.main.ui)
  
  implementation(libs.androidx.core.ktx)
  
  implementation(libs.androidx.lifecycle.runtime.ktx)
  
  implementation(libs.androidx.compose.runtime)
  
  implementation(libs.androidx.activity.compose)
  implementation(platform(libs.androidx.compose.bom))
  implementation(libs.androidx.profileinstaller)
  
  debugImplementation(libs.okhttp.logging.interceptor)
  
  implementation(libs.dagger.hilt.android)
  kapt(libs.dagger.hilt.compiler)
}
